package db4o_sistema;

import java.util.Scanner;

public class Cajero {
	static Scanner lee = new Scanner(System.in);
	static ConexionCaj cc = new ConexionCaj();
	static Administracion Ad = new Administracion();
	
	
	public static void main(String[] args) {
		
		Administracion a = new Administracion();
		int num_registro = 0;
		
		System.out.println("Registro de Usuario");
		System.out.println("�Cantidad de usuarios a registrar? =");
		 num_registro = lee.nextInt();
		 
		 //ingresa los datos requeridos
		 for(int i = 0; i < num_registro; i++) {
			System.out.println("Registra los datos del usuario");
			System.out.println("id:");
				a.setId(lee.nextInt());
			System.out.print("Nombre: ");
            	a.setNombre(lee.next());
            System.out.print("ApellidoPat");
            a.setApellidoPat(lee.next());
            System.out.print("ApellidoMat");
            a.setApellidoMat(lee.next());
            System.out.print("Nip");
            a.setNip(lee.nextInt());
            System.out.print("NumTar");
            a.setNumTar(lee.nextInt());
            System.out.print("FondoInicial");
            a.setFonIn(lee.nextDouble());
            System.out.print("Edad");
            a.setEdad(lee.nextInt());
            
            cc.insertarRegistro(a);             	
		 }
		 // aqui se genera un nuevo objeto que nos sirve para validar  
		 
		 System.out.print("NumTar");
         Ad.setNumTar(lee.nextInt());
         //manda llamar el metodo que devuelve un objeto
         Ad = cc.seleccionarAdministracion(Ad);
       
         
         
         //requiero nuevamente del nip
         System.out.print("Ingresa de nuevo tu Nip");
         int Nip = lee.nextInt();
         int op = 0;
         if(Ad.getNip() == Nip ) {
        	 //ingreso el menu para que pueda realizar las acciones requeridas o necesarias del usuario de la tarjeta 
        	do {
        	 System.out.println("-----MENU-----");
        	 System.out.println("1.Vasualizacion de Fondos");
        	 System.out.println("2.Deposito");
        	 System.out.println("3.Retiro");
        	 System.out.println("4.Modificar NIP");
        	 
        	 int menu = lee.nextInt();
        	 
        	 //actualizacion de fondos
        	 Administracion A2= new Administracion();
     		A2.setId(Ad.getId());
     		Ad = cc.seleccionarAdministracion(A2);
        	 
        	 switch(menu) {
        	 case 1:
        	 	System.out.println("EL SALDO ES:" + Ad.getFonIn());
        	 	break;
        	 	
        	 case 2:
        		 depositarDinero();
        		 break;
        		 
        	 case 3:
        	 	retirarDinero();
        	 	break;
        	 case 4:
        		 CambioNip();
        		 break;
        	 }
        	 
        	 System.out.print("�Desea elegir otra opcion del men�? {presiona 7}");
        	 
        	op = lee.nextInt();
        	}while(op == 7);
        	
        	
        	 
        	
         }else {
        	 System.out.print("El Nip no es el correcto");
         }
         
	}
	
	public static void depositarDinero() {
		//actualizar los datos del objeto
		Administracion A = new Administracion();
		A.setId(Ad.getId());
		Ad = cc.seleccionarAdministracion(A);
		//realiza deposito
		System.out.print("Ingresa el dinero a depositar");
		double cantidadDepositar = lee.nextDouble();
		if(cantidadDepositar > 0) {
			//devuelve el fondo que tiene el usuario y suma la cantidad a depositar 
			double fonIn = Ad.getFonIn() + cantidadDepositar;
			cc.actualizarAdministracion(Ad.getId(),fonIn);
		}
		
	}
	
	public static void retirarDinero() {
		
		
		//realiza retiro
		System.out.print("Ingresa la cantidad a retirar");
		double cantidadRetirar = lee.nextDouble();
		if(cantidadRetirar > 0 && cantidadRetirar <=Ad.getFonIn()) {
			
			//muestra la cantidad de los fondos y le resta la cantidad a Retirar
			double fonIn = Ad.getFonIn() - cantidadRetirar;
			cc.actualizarAdministracion(Ad.getId(),fonIn);
		}
		
	}
	
	public static void CambioNip() {
		//actualizar los datos del objeto
		Administracion A = new Administracion();
		A.setId(Ad.getId());
		Ad = cc.seleccionarAdministracion(A);
		//realiza el cambio del nip
		System.out.print("Ingresa el nuevo Nip");
		int CambioNip = lee.nextInt();
		if(CambioNip > 0) {
			
			cc.actualizarAdministracion(Ad.getId(),CambioNip);

		}
	}
}

