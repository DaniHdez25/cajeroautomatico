package db4o_sistema;

public class Administracion {
	//declarar las variables que se van a utilizar y ponerles a que elemento pertenecen
	private int id, Nip, NumTar, edad;
	private double FonIn;
	private String nombre, ApellidoPat, ApellidoMat;
	
	//una vez creado las varaiables se generan los get y los set para poder mandarlos a llamar en los diferentes requerimientos 
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPat() {
		return ApellidoPat;
	}
	public void setApellidoPat(String apellidoPat) {
		ApellidoPat = apellidoPat;
	}
	public String getApellidoMat() {
		return ApellidoMat;
	}
	public void setApellidoMat(String apellidoMat) {
		ApellidoMat = apellidoMat;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public int getNip() {
		return Nip;
	}
	public void setNip(int nip) {
		Nip = nip;
	}
	public int getNumTar() {
		return NumTar;
	}
	public void setNumTar(int numTar) {
		NumTar = numTar;
	}
	public double getFonIn() {
		return FonIn;
	}
	public void setFonIn(double fonIn) {
		FonIn = fonIn;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	//convierte a una cadena de texto 
	@Override
	public String toString() {
		return "Administracion [id=" + id + ", Nip=" + Nip + ", NumTar=" + NumTar + ", edad=" + edad + ", FonIn="
				+ FonIn + ", nombre=" + nombre + ", ApellidoPat=" + ApellidoPat + ", ApellidoMat=" + ApellidoMat + "]";
	}
	
	
	
	

	
	
	

}
