package db4o_sistema;

import com.db4o.Db4oEmbedded;
import com.db4o.EmbeddedObjectContainer;
import com.db4o.ObjectSet;


public class ConexionCaj { 
	
    private EmbeddedObjectContainer db = null;
    
    private void AbrirRegistro(){
        db = Db4oEmbedded.openFile("registroUsuario"); //Abrir registro y crear bd que es un archivo
    }
    
    private void CerrarRegistro(){
        db.close();
    }
    
    public void insertarRegistro(Administracion a){
        AbrirRegistro();
        db.store(a);
        CerrarRegistro();
    }
    //hace una consulta de una entidad de administracion  con el criterio del objeto que se pase
    public Administracion seleccionarAdministracion(Administracion Ad) {
    	try {
    		AbrirRegistro();
    		ObjectSet resultado = db.queryByExample(Ad);
    		if(resultado.hasNext()) {
    			Administracion Administracion =(Administracion) resultado.next();
    			CerrarRegistro();
    			return Administracion; 			
    		}
    	}
    	//mostrar un mensaje que indique que ese criterio no es el correcto
    	catch(Exception e){
    		System.out.println("Ocurrio un error, seleccione su tarjeta");		
    	}
    	CerrarRegistro();
    	return null;
    	
    	
    }
    
    public void actualizarAdministracion(int id, double fonIn) {
    	try {
    		AbrirRegistro();
    		Administracion administracion = new Administracion();
    		administracion.setId(id);
    		ObjectSet resultado = db.queryByExample(administracion);
    		if(resultado.hasNext()) {
    			//hace referencia a la clase
    			Administracion preResultado = (Administracion) resultado.next();
    			preResultado.setFonIn(fonIn);
    			System.out.print(preResultado);
    			db.store(preResultado);
    			CerrarRegistro();
    			
    			
    		}
    		
    		
    	}catch(Exception e) {
    		System.out.println("Se realizo una Actualizacion a la tarjeta");
    	}
    	
    	
    }

    public void actualizarAdministracion(int id, int Nip) {
    	try {
    		AbrirRegistro();
    		Administracion administracion = new Administracion();
    		administracion.setId(id);
    		ObjectSet resultado = db.queryByExample(administracion);
    		if(resultado.hasNext()) {
    			//hace referencia a la clase
    			Administracion preResultado = (Administracion) resultado.next();
    			preResultado.setNip(Nip);
    			System.out.print(preResultado);
    			db.store(preResultado);
    			CerrarRegistro();
    		}
    
    	}catch(Exception e) {
    		System.out.println("Se realizo una Actualizacion a la tarjeta");
    	}
    }
    

}
    
